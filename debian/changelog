libjlayer-java (1.0.1-3) unstable; urgency=medium

  * Team upload.
  * Switch to the source format 3.0 (quilt)
  * Build with the DH sequencer instead of CDBS
  * Install the Maven artifacts
  * Removed the -doc package
  * Removed Damien Raude-Morvan from the uploaders (Closes: #889355)
  * Updated the homepage
  * Standards-Version updated to 4.6.0.1
  * Switch to debhelper level 13
  * Updated the watch file
  * Refreshed debian/copyright

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 15 Mar 2022 10:59:43 +0100

libjlayer-java (1.0.1-2) unstable; urgency=low

  * debian/control:
    + Remove dependency on default-jre-headless, since java libs aren't
      supposed to depend on JREs.
    + Bumped Standards-Version to 3.9.1

 -- أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@sabily.org>  Mon, 01 Nov 2010 14:17:40 +0200

libjlayer-java (1.0.1-1) unstable; urgency=low

  * New upstream release.
  * Split documentation into libjlayer-java-doc package:
    - Install API documentation in /usr/share/doc/libjlayer-java/api/
  * Bump Standards-Version to 3.8.3
    - Change section to "java"
    - Rename debian/README.Debian-source to debian/README.source
  * Bump debhelper version to >= 7
  * Remove unused Depends on ${shlibs:Depends}
  * Default JRE:
    - Build-Depends on default-jdk
    - Depends on default-jre-headless
    - Use /usr/lib/jvm/default-java as JAVA_HOME
    - Force 1.2 source and target in debian/ant.properties
  * Add myself to Uploaders
  * Use DEP5 format for debian/copyright

 -- Damien Raude-Morvan <drazzib@debian.org>  Sun, 01 Nov 2009 22:15:00 +0100

libjlayer-java (1.0-2) unstable; urgency=low

  * Move the package to pkg-java svn.
  * Switch to java-gcj-compat from kaffe.
  * debian/control:
    + Set Maintainer to Debian Java Maintainers and add myself to Uploaders.
    + Add XS-Vcs-{Svn,Browser} headers.
  * Remove debian/install and install jar from debian/rules and create
    symbolic link.
  * Add debian/README.Debian-source file.
  * Bump up Standards-Version to 3.7.3

 -- Varun Hiremath <varun@debian.org>  Fri, 11 Jan 2008 10:28:56 +0530

libjlayer-java (1.0-1) unstable; urgency=low

  * Initial release (Closes: #420543)

 -- Varun Hiremath <varunhiremath@gmail.com>  Mon, 23 Apr 2007 00:21:24 +0530
